﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Archiver
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите строку:");
            string input = Console.ReadLine();
            HuffmanTree huffmanTree = new HuffmanTree();

            // Построение дерева Хаффмана
            huffmanTree.Build(input);

            // Кодирование
            BitArray encoded = huffmanTree.Encode(input);

            Console.Write("Закодированый текст: ");
            foreach (bool bit in encoded)
            {
                Console.Write((bit ? 1 : 0) + "");
            }
            Console.WriteLine();

            // Декодирование
            string decoded = huffmanTree.Decode(encoded);

            Console.WriteLine("Декодированый текст: " + decoded);

            Console.ReadLine();
        }
    }
}